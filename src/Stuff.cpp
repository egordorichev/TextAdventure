#include "Stuff.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include <stdlib.h>

std::string enemyNames[3] = {
	"Buster",
	"Warrior",
	"Hammer"
};

void clear(){
	std::cout << std::string(100, '\n');
}

void pause(){
	std::cout << BOLDGREEN << "\nPress enter to continue\n\033[0m" << RESET;
	std::cin.get();
	std::cin.get();
}

int showMenu(std::string question, std::vector <std::string> variants){
	std::cout << question << "\n\n";

	for(int i = 0; i < variants.size(); i++){
		std::cout << BOLDGREEN << i + 1 << ") " << RESET << variants[i] << "\n";
	}

	std::cout << BOLDWHITE << "\n> " << RESET;

	int answer;

	std::cin >> answer;

	return answer; // FIXME: get one char
}

std::string toLower(std::string str){
	std::transform(str.begin(), str.end(), str.begin(), ::tolower); // To lower

	return str;
}

std::string randomName(){
	return enemyNames[rand() % 2 + 0];
}