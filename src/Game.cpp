#include "Game.hpp"
#include "Player.hpp"
#include "Stuff.hpp"
#include "Item.hpp"

#include <fstream>
#include <jsoncpp/json/json.h>

Game::Game(){
	this->difficulty = 1;
}

Game::~Game(){
	this->save();
}

void Game::start(std::string playerName, int race, int difficulty){
	this->player = Player(playerName, race);
	this->difficulty = difficulty;
	this->start(this->difficulty);
}

void Game::start(int difficulty){
	clear();

	int state = 0;
	int choise = 0;
	this->location = BAR;

	while(state == 0){
		std::vector <std::string> items;
		std::string itemDescription;

		switch(location){
			case BAR:
				switch(showMenu("\033[1m\033[37m\nYour are in the bar\033[0m", { "Explore outside",
					"Rest", "Go to the shop", "Show statics", "End your Journey" })){
						case 1:
							location = FOREST;
						break;
						case 2:
							std::cout << "\nYou slept a few hour and now you are refreshed.\n";

							player.setHP(player.getMaxHP());
							player.setSP(player.getMaxSP());
						break;
						case 3:
							this->location = SHOP;
						break;
						case 4:
							this->player.printStatics();
						break;
						case 5:
							state = 1;
						break;
						default: break;
					}
			break;
			case SHOP:
				switch(showMenu("\033[1m\033[37m\nYour are in the shop\033[0m", { "Buy item",
					"Go back to bar", "Show statics", "End your Journey" })){
					case 1:
						for(int i = 0; i < this->itemManager.getSize(); i++){
							itemDescription = (this->itemManager.getItem(i)->isBought()) ? ((this->player.getSword()->getId() == this->itemManager.getItem(i)->getId() ||
								this->player.getArmorSet()->getId() == this->itemManager.getItem(i)->getId()) ? BOLDYELLOW : BOLDGREEN) :
								((this->player.getGold() >= this->itemManager.getItem(i)->getCost()) ? BOLDWHITE : BOLDRED);

							itemDescription += this->itemManager.getItem(i)->getName() + RESET;
							itemDescription += ": " + this->itemManager.getItem(i)->getDescription() + "\n\t";
							itemDescription += BOLDWHITE;
							itemDescription += std::to_string(this->itemManager.getItem(i)->getDamage());
							itemDescription += RESET;
							itemDescription += " damage, ";
							itemDescription += BOLDWHITE;
							itemDescription += std::to_string(this->itemManager.getItem(i)->getDefense());
							itemDescription += RESET;
							itemDescription += " defense. Cost: ";
							itemDescription += BOLDWHITE;
							itemDescription += std::to_string(this->itemManager.getItem(i)->getCost());
							itemDescription += RESET;
							itemDescription += " gold.";

							items.push_back(itemDescription);
						}

						choise = showMenu("\nAvailable items", items) - 1;

						if(this->itemManager.getItem(choise)->isBought()){
							std::cout << BOLDGREEN << "\nYou selected " + this->itemManager.getItem(choise)->getName() + "!\n" << RESET;

							if(this->itemManager.getItem(choise)->getType() == SWORD){
								this->player.setSword(this->itemManager.getItem(choise));
							} else {
								this->player.setArmorSet(this->itemManager.getItem(choise));
							}
						} else if(this->player.getGold() >= this->itemManager.getItem(choise)->getCost()){
							std::cout << BOLDGREEN << "\nYou bought " + this->itemManager.getItem(choise)->getName() + "!\n" << RESET;

							this->player.useGold(this->itemManager.getItem(choise)->getCost());
							this->itemManager.getItem(choise)->setBought(true);

							if(this->itemManager.getItem(choise)->getType() == SWORD){
								this->player.setSword(this->itemManager.getItem(choise));
							} else {
								this->player.setArmorSet(this->itemManager.getItem(choise));
							}
						} else {
							std::cout << BOLDRED << "\nNot enough gold!\n" << RESET;
						}

						choise = 0;
					break;
					case 2:
						this->location = BAR;
					break;
					case 3:
						this->player.printStatics();
					break;
					case 4:
						state = 1;
					break;
					default: break;
				}
			break;
			case FOREST:
				switch(showMenu("\033[1m\033[37m\nYour are in a forest\033[0m", { "Move",
					"Go back to bar", "Show statics", "End your Journey" })){
					case 1:
						this->randomEvent();
					break;
					case 2:
						this->location = BAR;
					break;
					case 3:
						this->player.printStatics();
					break;
					case 4:
						state = 1;
					break;
					default: break;
				}
			break;
		}
	}
}

void Game::save(){
	Json::Reader reader;
	Json::Value root;

	std::ifstream config("./data.json", std::ios::binary);
	reader.parse(config, root, false);
	config.close();

	root["player"]["sword"] = this->player.getSword()->getId();
	root["player"]["armorSet"] = this->player.getArmorSet()->getId();
	root["player"]["name"] = this->player.getName();
	root["player"]["race"] = this->player.getRaceAsInt();
	root["player"]["maxHP"] = this->player.getMaxHP();
	root["player"]["maxSP"] = this->player.getMaxSP();
	root["player"]["level"] = this->player.getLevel();
	root["player"]["gold"] = this->player.getGold();
	root["player"]["experience"] = this->player.getExperience();
	root["player"]["dodge"] = this->player.getDodge();
	root["player"]["maxExperience"] = this->player.getMaxExperience();
	root["player"]["attackStrengthMin"] = this->player.getAttackStrengthMinWithoutItems();
	root["player"]["attackStrengthMax"] = this->player.getAttackStrengthMaxWithoutItems();
	root["player"]["defense"] = this->player.getDefenseWithoutItems();
	root["game"]["difficulty"] = this->difficulty;

	std::ofstream data("./data.json", std::ios::binary | std::ios::trunc);
	data << root << "\n";
	data.close();

	this->itemManager.save();
}

void Game::randomEvent(){
	int chance = rand() % 100 + 1;

	if(chance >= 50 + this->difficulty * 10){
		int defense = rand() % ((player.getAttackStrengthMin() - 10) - (player.getAttackStrengthMin() - 20) + 1) + (player.getAttackStrengthMin() - 20);
		int strength = rand() % ((player.getDefense() + 10) - (player.getDefense()) + 1) + (player.getDefense());

		Player enemy(randomName(), rand() % 4 + 1);

		if(chance < 90){
			std::cout << "\nAn enemy has appeared!\n";

			enemy.setAttackStrengthMax(strength + 15);
			enemy.setAttackStrengthMin(strength);
			enemy.setDefense(defense);
		} else {
			std::cout << BOLDRED << "\nBoss has appeared!\n" << RESET;

			enemy.setGold(100);
			enemy.setAttackStrengthMax(100);
			enemy.setAttackStrengthMin(100);
			enemy.setDefense(50);
		}

		this->player.printStatics();
		enemy.printStatics();

		std::cout << "\n";

		bool escape = false;

		while(!this->player.isDead() && !enemy.isDead() && !escape){
			switch(showMenu("What you will do?", { "Attack", "Use a skill", "Use an item", "Run away" })){
				case 1:
					std::cout << "\nYou attacked " << enemy.getName() << "! ";
					this->player.attack(&enemy);
				break;
				case 2:
					// TODO
				break;
				case 3:
					// TODO
				break;
				case 4:
					std::cout << "\nYou had escaped!\n"; // TODO: mesuare speeds

					escape = true;
				break;
			}

			pause();

			if(!enemy.isDead() && !escape){
				std::cout << enemy.getName() << " attacks you! ";
				enemy.attack(&this->player);
				std::cout << "\n";
			}
		}

		if(enemy.isDead()){
			std::cout << BOLDRED << enemy.getName() << " is defeated!\n\n" << RESET << "You received " << BOLDWHITE << enemy.getExperience() << RESET << " experience and " << BOLDWHITE << enemy.getGold() << RESET << " gold!\n";

			this->player.addGold(enemy.getGold());

			if(this->player.levelUp(enemy.getExperience())){
				std::cout << BOLDGREEN << "You leveled up!\n" << RESET;
			}
		} else if(this->player.isDead()){
			std::cout << BOLDRED << "You died!\nYou lost half of your gold and experience points!\n" << RESET;

			this->player.setGold(this->player.getGold() / 2);
			this->player.setExperience(this->player.getExperience() / 2);
			this->player.setHP(this->player.getMaxHP());
			this->player.setSP(this->player.getMaxSP());
		}
	}
}