#include <iostream>
#include <ctime>
#include <string>
#include <vector>
#include <fstream>

#include <jsoncpp/json/json.h>

#include "Game.hpp"
#include "Stuff.hpp"

int main(){
	Game game;

	srand(time(NULL));

	clear();

	int choise;

	choise = showMenu("Wellcome to TextAdventure!\nWould you like to load existing game\nor start new one?", { "New game", "Load game", "Exit" });

	if(choise == 2){
		Json::Value root;
		Json::Reader reader;

		std::ifstream config("data.json", std::ios::binary);
		reader.parse(config, root, false);

		Player player(root["player"]["name"].asString(), root["player"]["race"].asInt());

		player.setMaxHP(root["player"]["maxHP"].asInt());
		player.setMaxSP(root["player"]["maxSP"].asInt());
		player.setLevel(root["player"]["level"].asInt());
		player.setGold(root["player"]["gold"].asInt());
		player.setExperience(root["player"]["experience"].asInt());
		player.setDodge(root["player"]["dodge"].asInt());
		player.setMaxExperience(root["player"]["maxExperience"].asInt());
		player.setAttackStrengthMin(root["player"]["attackStrengthMin"].asInt());
		player.setAttackStrengthMax(root["player"]["attackStrengthMax"].asInt());
		player.setSword(game.itemManager.getItem(root["player"]["sword"].asInt()));
		player.setArmorSet(game.itemManager.getItem(root["player"]["armorSet"].asInt()));

		config.close();

		game.setPlayer(player);
		game.start(root["game"]["difficulty"].asInt());
	} else if(choise == 1){
		std::string name;

		int race;
		int difficulty;

		std::cout << "\nWhat is your name, stranger? ";
		std::cin >> name;

		race = showMenu(name + ", please, pick your race:", { "Human", "Orc", "Elf", "Gnome" });

		difficulty = showMenu("\nAnd the last thing. Pick difficulty for this game:", { "Easy", "Medium", "Hard (only for players, who like to die)" });

		std::cout << "\nYou better move fast, " << name << ". The big war is comming!\n";

		Player player(name, race);

		player.setSword(game.itemManager.getItem(0));
		player.setArmorSet(game.itemManager.getItem(3));

		pause();

		game.setPlayer(player);
		game.start(difficulty);
	} else if(choise == 3){
		return 0;
	} else {
		std::cout << "Sorry, command you entered isn't a real choise\n";
	}

	return 0;
}
