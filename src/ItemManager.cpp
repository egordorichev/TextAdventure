#include "ItemManager.hpp"

#include <fstream>

#include <jsoncpp/json/json.h>

ItemManager::ItemManager(){
	Json::Value root;
	Json::Reader reader;

	std::ifstream config("items.json", std::ifstream::binary);
	reader.parse(config, root, false);

	for(auto item : root["items"]){
		this->items.push_back(new Item((item["type"].asInt() == 2) ? ARMOR_SET : SWORD, item["id"].asInt(),  item["name"].asString(), item["description"].asString(), item["defense"].asInt(), item["damage"].asInt(), item["cost"].asInt(), item["useable"].asBool(), item["bought"].asBool()));
	}

	config.close();
}

Item *ItemManager::getItem(int id){
	return this->items[id];
}

void ItemManager::save(){
	Json::Reader reader;
	Json::Value root;

	std::ifstream config("./items.json", std::ios::binary);
	reader.parse(config, root, false);
	config.close();

	std::ofstream data("./items.json", std::ios::binary | std::ios::trunc);

	for(int i = 0; i < this->getSize(); i++){
		root["items"][i]["bought"] = this->items[i]->isBought();
	}

	data << root << "\n";
	data.close();
}