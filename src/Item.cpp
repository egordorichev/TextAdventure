#include "Item.hpp"

#include <iostream>
#include <string>

Item::Item(ItemType type, int id, std::string name, std::string description, int defense, int damage, int cost, bool useable, bool bought){
	this->type = type;
	this->name = name;
	this->id = id;
	this->description = description;
	this->defense = defense;
	this->damage = damage;
	this->cost = cost;
	this->useable = useable;
	this->bought = bought;
}