#include "Player.hpp"
#include "Stuff.hpp"

#include <cstdlib>

Player::Player(std::string name, int race){
	this->sword = NULL;
	this->armorSet = NULL;

	this->name = name;
	this->race = race;
	this->experience = rand() % 50 + 10;
	this->level = 1;
	this->gold = rand() % 50 + 10;
	this->maxExperience = 100;

	switch(this->race){
		case 1:
			this->maxHP = 50;
			this->maxSP = 5;
			this->defense = 3;
			this->attackStrengthMin = 5;
			this->attackStrengthMax = 10;
			this->dodge = 20;
		break;
		case 2:
			this->maxHP = 30;
			this->maxSP = 2;
			this->defense = 5;
			this->attackStrengthMin = 15;
			this->attackStrengthMax = 35;
			this->dodge = 25;
		break;
		case 3:
			this->maxSP = 15;
			this->maxHP = 60;
			this->defense = 3;
			this->attackStrengthMin = 3;
			this->attackStrengthMax = 8;
			this->dodge = 35;
		break;
		case 4:
			this->maxSP = 10;
			this->maxHP = 45;
			this->defense = 5;
			this->attackStrengthMin = 8;
			this->attackStrengthMax = 15;
			this->dodge = 30;
		break;
	}

	this->HP = this->maxHP;
	this->SP = this->maxSP;
}

void Player::printStatics(){
	std::cout << "\n\033[1m\033[37m" << this->getName() << " (" << this->getRace() << ")\033[0m\n\n"
	<< "HP: " << this->getHP() << "/" << this->getMaxHP() << "\n"
	<< "SP: " << this->getSP() << "/" << this->getMaxSP() << "\n"
	<< "Level: " << this->getLevel() << "\n"
	<< "Experience: " << this->getExperience() << "/" << this->getMaxExperience() << "\n"
	<< "Defense: " << this->getDefense() << "\n"
	<< "Damage: " << this->getAttackStrengthMin() << "-" << this->getAttackStrengthMax() << "\n"
	<< "Dodge: " << this->getDodge() << "\n"
	<< "Gold: " << this->getGold() << "\n";
}

std::string Player::getRace(){
	switch(this->race){
		case 1: return "Human";
		case 2: return "Orc";
		case 3: return "Elf";
		case 4: return "Gnome";
		default: return "Unknown";
	}
}

void Player::heal(int HP){
	if(this->maxHP < HP + this->HP){
		this->HP = this->maxHP;
	} else {
		this->HP += HP;
	}
}

void Player::attack(Player *player){
	int damage = rand() % (this->attackStrengthMax - this->attackStrengthMin + 1) + this->attackStrengthMin;

	player->reciveDamage(damage);
}

int Player::getDefenseWithoutItems(){
		int result = this->defense;

		if(this->sword != NULL){
			result -= this->sword->getDefense();
		}

		if(this->armorSet != NULL){
			result -= this->armorSet->getDefense();
		}

		return result;
}

int Player::getAttackStrengthMaxWithoutItems(){
		int result = this->attackStrengthMax;

		if(this->sword != NULL){
			result -= this->sword->getDamage();
		}

		if(this->armorSet != NULL){
			result -= this->armorSet->getDamage();
		}

		return result;
}

int Player::getAttackStrengthMinWithoutItems(){
	int result = this->attackStrengthMin;

	if(this->sword != NULL){
		result -= this->sword->getDamage();
	}

	if(this->armorSet != NULL){
		result -= this->armorSet->getDamage();
	}

	return result;
}

void Player::setSword(Item *sword){
	if(this->sword != NULL){
		this->attackStrengthMax -= this->sword->getDamage();
		this->attackStrengthMin -= this->sword->getDamage();
		this->defense -= this->sword->getDefense();
	}

	this->sword = sword;

	this->attackStrengthMin += sword->getDamage();
	this->attackStrengthMax += sword->getDamage();
	this->defense += sword->getDefense();
}

void Player::setArmorSet(Item *armorSet){
	if(this->armorSet != NULL){
		this->attackStrengthMax -= this->armorSet->getDamage();
		this->attackStrengthMin -= this->armorSet->getDamage();
		this->defense -= this->armorSet->getDefense();
	}

	this->armorSet = armorSet;

	this->attackStrengthMin += this->armorSet->getDamage();
	this->attackStrengthMax += this->armorSet->getDamage();
	this->defense += this->armorSet->getDefense();
}

bool Player::reciveDamage(int damage){
	int fortune = rand() % 100;

	if(this->defense < damage && fortune > this->dodge){
		this->HP -= damage - defense;

		std::cout << this->name << " took " << BOLDWHITE << damage - defense << RESET << " damage!\n";

		if(this->HP < 0){
			this->HP = 0;
		}
	} else if(this->defense >= damage){
		std::cout << "Аrmor is not breached!" << std::endl;
	} else {
		std::cout << this->name << " avoided attack!\n";
	}
}

void Player::addGold(int gold){
	this->gold += gold;
}

bool Player::useGold(int gold){
	if(this->gold >= gold){
		this->gold -= gold;

		return true;
	} else {
		this->gold -= gold;

		return false;
	}
}

bool Player::useSP(int SP){
	if(this->SP >= SP){
		this->SP -= SP;

		return true;
	} else {
		return false;
	}
}

bool Player::levelUp(int experience){
	if(this->experience + experience < 0){
		this->experience = 0;
	} else if(this->experience + experience >= this->maxExperience){
		this->level++;
		this->experience += experience - this->maxExperience;
		this->maxExperience *= 1.5;

		return true;
	} else {
		this->experience += experience;
	}

	return false;
}

bool Player::isDead(){
	return (this->HP == 0);
}