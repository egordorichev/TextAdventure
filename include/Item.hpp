#ifndef ITEM_HPP_
#define ITEM_HPP_

#include <iostream>
#include <string>

enum ItemType{
	SWORD,
	ARMOR_SET
};

class Item{
	public:
		Item(ItemType type, int id, std::string name, std::string description, int defense, int damage, int cost, bool useable, bool bought);

		std::string getName(){ return this->name; };
		std::string getDescription(){ return this->description; };

		ItemType getType(){ return this->type; };

		void setBought(bool bought){ this->bought = bought; };

		int getDefense(){ return this->defense; };
		int getDamage(){ return this->damage; };
		int getCost(){ return this->cost; };
		int getId(){ return this->id; };

		bool isUseable(){ return this->useable; };
		bool isBought(){ return this->bought; };
	private:
		ItemType type;

		std::string name;
		std::string description;

		int damage;
		int defense;
		int cost;
		int id;

		bool useable;
		bool bought;
};

#endif // ITEM_HPP_