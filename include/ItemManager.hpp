#ifndef ITEMMANAGER_HPP_
#define ITEMMANAGER_HPP_

#include "Item.hpp"

#include <iostream>
#include <string>
#include <vector>

class ItemManager{
	public:
		ItemManager();

		Item *getItem(int id);
		int getSize(){ return this->items.size(); };
		void save();
	private:
		std::vector <Item *> items;
};

#endif // ITEMMANAGER_HPP_