#ifndef GAME_HPP_
#define GAME_HPP_

#include "Player.hpp"
#include "Stuff.hpp"
#include "ItemManager.hpp"

#include <string>
#include <iostream>

enum Location{
	BAR,
	SHOP,
	FOREST
};

class Game{
	public:
		Game();
		~Game();

		void setPlayer(Player player){ this->player = player; };
		void start(int difficulty);
		void start(std::string playerName, int race, int difficulty);
		void save();
		void randomEvent();

		ItemManager itemManager;
	private:
		Player player;
		Location location;

		int difficulty;
};

#endif // GAME_HPP_
