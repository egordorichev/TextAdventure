#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Item.hpp"

#include <string>
#include <iostream>

class Player{
	public:
		Player(std::string name, int race);
		Player(){};

		Item *getSword(){ return this->sword; };
		Item *getArmorSet(){ return this->armorSet; };

		int getRaceAsInt(){ return this->race; }
		int getDefense(){ return this->defense; };
		int getHP(){ return this->HP; };
		int getSP(){ return this->SP; };
		int getMaxHP(){ return this->maxHP; };
		int getMaxSP(){ return this->maxSP; };
		int getExperience(){ return this->experience; };
		int getLevel(){ return this->level; };
		int getGold(){ return this->gold; };
		int getAttackStrengthMin(){ return this->attackStrengthMin; };
		int getAttackStrengthMax(){ return this->attackStrengthMax; };
		int getMaxExperience(){ return this->maxExperience; };
		int getDodge(){ return this->dodge; };

		int getAttackStrengthMaxWithoutItems();
		int getAttackStrengthMinWithoutItems();
		int getDefenseWithoutItems();

		void setDefense(int defense){ this->defense = defense; };
		void setHP(int HP){ this->HP = HP; };
		void setSP(int SP){ this->SP = SP; };
		void setMaxHP(int HP){ this->maxHP = HP; this->HP = HP; };
		void setMaxSP(int SP){ this->maxSP = SP; this->SP = SP; };
		void setExperience(int experience){ this->experience = experience; };
		void setLevel(int level){ this->level = level; };
		void setGold(int gold){ this->gold = gold; };
		void setAttackStrengthMin(int strengthMin){ this->attackStrengthMin = strengthMin; };
		void setAttackStrengthMax(int strengthMax){ this->attackStrengthMax = strengthMax; };
		void setMaxExperience(int experience){ this->maxExperience = experience; };
		void setDodge(int dodge){ this->dodge = dodge; };

		void heal(int HP);
		void attack(Player *player);
		void addGold(int gold);
		void printStatics();
		void setSword(Item *sword);
		void setArmorSet(Item *armorSet);

		std::string getRace();
		std::string getName(){ return this->name; };

		bool reciveDamage(int damage);
		bool useGold(int gold);
		bool useSP(int SP);
		bool levelUp(int experience);
		bool isDead();
	private:
		Item *sword;
		Item *armorSet;

		std::string name;

		int race;
		int defense;
		int HP;
		int SP;
		int maxSP;
		int maxHP;
		int experience;
		int level;
		int gold;
		int attackStrengthMin;
		int attackStrengthMax;
		int maxExperience;
		int dodge;
};

#endif // PLAYER_HPP_
